package hackyeah.paragraf.paragrafs

data class Paragraf(
        val url: String,
        val content: String,
        val tag1: String,
        val tag2: String,
        val tag3: String

        )
package hackyeah.paragraf.paragrafs


import ParagrafsRecyclerViewAdapter
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import hackyeah.paragraf.R
import kotlinx.android.synthetic.main.fragment_paragraf_list.*
import kotlinx.android.synthetic.main.fragment_paragraf_list.view.*

class ParagrafFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_paragraf_list, container, false)
        view.search_button.setOnClickListener{
            var keywords = parseKeywords(search_keywords.text.toString())
            val paragrafList = findArticles(keywords)
            progressBar.visibility = View.VISIBLE
            var k = 0
            while(k < 2000000000){
                k++
        }
            progressBar.visibility = View.INVISIBLE
            if (view.list is RecyclerView) {
                view.list.adapter = ParagrafsRecyclerViewAdapter(paragrafList, context!!)
            }
        }

        return view
    }
    private fun parseKeywords(keywords:String): List<String> {
        val keyword_lists = keywords.split(";")
        return keyword_lists.map { it.toLowerCase()}
    }

    fun findArticles(keywords: List<String>): List<Paragraf>{
//    TODO: Implement finding articles request
        return listOf(
            Paragraf(
                "http://prawo.sejm.gov.pl/isap.nsf/download.xsp/WDU20200000569/O/D20200569.pdf",
                "U. z 2019 r. poz. 1398 oraz 2020r. poz. 148, 284 i374), w tym skutków rozprzestrzeniania się COVID 19, w szczególności poprzez: a) udzielanie wsparcia finansowego",
                "Szkolnictwo",
                "Wychowanie",
                "Ochrona Zdrowia"
            ),
            Paragraf(
                "http://prawo.sejm.gov.pl/isap.nsf/download.xsp/WDU20200000583/O/D20200583.pdf",
                "ustawy z dnia 2 marca 2020r. o szczególnych rozwiązaniach związanych z zapobieganiem, przeciwdziałaniemi zwalczaniem COVID 19, innych chorób zakaźnych",
                "Fundusze",
                "Ubezpieczenia",
                "Inwestycje"
            ),
            Paragraf(
                "http://prawo.sejm.gov.pl/isap.nsf/download.xsp/WDU20200000530/O/D20200530.pdf",
                "szczególnych rozwiązań w okresie czasowego ograniczenia funkcjonowaniajednostek systemu oświaty w związku z zapobieganiem, przeciwdziałaniem i zwalczaniem COVID",
                "Choroby Zakaźne",
                "Oświata",
                "Szkolnictwo Wyższe"
            )
            )
    }
}

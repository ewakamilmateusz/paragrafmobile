import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import hackyeah.paragraf.R
import hackyeah.paragraf.paragrafs.Paragraf
import kotlinx.android.synthetic.main.fragment_paragraf.view.*


class ParagrafsRecyclerViewAdapter(
    private val paragrafList: List<Paragraf>,
    private val context: Context
) : RecyclerView.Adapter<ParagrafsRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.fragment_paragraf, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sponsor = paragrafList[position]

        holder.fill(sponsor)
    }

    override fun getItemCount(): Int = paragrafList.size

    class ViewHolder(private var sponsorView: View) : RecyclerView.ViewHolder(sponsorView) {

        fun fill(paragraf: Paragraf) {

            sponsorView.apply {
                open_paragraf_button.setOnClickListener {
                    val uri = Uri.parse(paragraf.url)

                    val intent = Intent(Intent.ACTION_VIEW, uri)
                    startActivity(context, intent, Bundle())

                }

                paragraf_content.text = paragraf.content
                paragraf_tag_1.text = paragraf.tag1
                paragraf_tag_2.text = paragraf.tag2
                paragraf_tag_3.text = paragraf.tag3

            }
        }
    }


}
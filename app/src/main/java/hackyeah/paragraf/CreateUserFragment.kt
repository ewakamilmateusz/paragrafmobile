package hackyeah.paragraf

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_create_user.view.*


class CreateUserFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_create_user, container, false)

        view.button_create_user.setOnClickListener {
            val email = view.create_user_email.text.trim().toString()
            val password = view.create_user_password.text.trim().toString()
            val passwordRepetition = view.create_user_password_repetition.text.trim().toString()

            val validEmail = validateEmail(email)
            val validPassword = validatePassword(password, passwordRepetition)
            var userCreated = false

            if (validEmail and validPassword) {
                userCreated = createUser(email, password)
            } else {
                when {
                    (!validEmail) -> Toast.makeText(
                        activity,
                        "Podaj poprawny adres email!",
                        Toast.LENGTH_LONG
                    ).show()

                    (!validPassword) -> Toast.makeText(
                        activity,
                        "Wprowadzone hasła nie są poprawne",
                        Toast.LENGTH_LONG
                    ).show()
                    else -> Toast.makeText(
                        activity,
                        "Nieznany problem, spróbuj jeszcze raz!",
                        Toast.LENGTH_LONG
                    ).show()
                }
            }
            if (userCreated){
                Toast.makeText(
                    activity,
                    "Gratuluję, konto zostało stworzone!",
                    Toast.LENGTH_LONG
                ).show()

                findNavController().navigate(R.id.action_CreateUserFragment_to_SecondFragment)
            }
        }

        return view
    }

    private fun validateEmail(email: String): Boolean{
        return email.contains('@') and (email.length > 5)
    }

    private fun validatePassword(password: String, passwordRepetition: String): Boolean{
        return password.isNotEmpty() and
                (password==passwordRepetition) and
                (password.length >= 8) and
                password.contains("[0-9]+".toRegex())
    }

    private fun createUser(email: String, password: String):Boolean {
//        TODO: Implement request for user creation.
        return true
    }
}

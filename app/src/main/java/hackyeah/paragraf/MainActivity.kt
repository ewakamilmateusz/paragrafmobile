package hackyeah.paragraf

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import hackyeah.paragraf.paragrafs.ParagrafFragment
import hackyeah.paragraf.tags.TagFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        subscribed_tags.setOnClickListener {
            val manager: FragmentManager = supportFragmentManager
            val transaction: FragmentTransaction = manager.beginTransaction()
            transaction.replace(R.id.nav_host_fragment, TagFragment())
            transaction.addToBackStack(null)
            transaction.commit()
        }

        search_paragrafs_button.setOnClickListener {
            val manager: FragmentManager = supportFragmentManager
            val transaction: FragmentTransaction = manager.beginTransaction()
            transaction.replace(R.id.nav_host_fragment, ParagrafFragment())
            transaction.addToBackStack(null)
            transaction.commit()
        }

    }


}

package hackyeah.paragraf

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_login.view.*
import androidx.navigation.fragment.findNavController

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class LoginFragment : Fragment() {

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_login, container, false)
        view.button_login.setOnClickListener {
            val email = view.login_email.text.trim().toString()
            val password = view.login_password.text.trim().toString()
            val token = getToken(email, password)

            if (token.isNotEmpty()){
                findNavController().navigate(R.id.action_SecondFragment_to_ParagrafFragment)
            }


        }
        return view
    }

    private fun getToken(email: String, password: String): String {
//        TODO: Implement login fuction
        return "MY_TOKEN"
    }
}

package hackyeah.paragraf.tags

data class Tag(
        val name: String,
        var isSubscribed: Boolean = false
        )

package hackyeah.paragraf.tags


import TagRecyclerViewAdapter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import hackyeah.paragraf.R
import kotlinx.android.synthetic.main.fragment_tag_list.view.*


class TagFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_tag_list, container, false)
        val paragrafList = listOf(
            Tag("Szkolnictwo", true),
            Tag("Wychowanie", false),
            Tag("Ochrona Zdrowia", true),
            Tag("Fundusze", false),
            Tag("Ubezpieczenia", false),
            Tag("Inwestycje", false),
            Tag("Choroby Zakaźne", false),
            Tag("Oświata", true)
            )
        if (view.tag_list is RecyclerView) {
            view.tag_list.adapter = TagRecyclerViewAdapter(paragrafList, context!!)

        }

        return view
    }
}

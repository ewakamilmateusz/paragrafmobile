import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hackyeah.paragraf.R
import hackyeah.paragraf.tags.Tag
import kotlinx.android.synthetic.main.fragment_tag.view.*


class TagRecyclerViewAdapter(
    private val paragrafList: List<Tag>,
    private val context: Context
) : RecyclerView.Adapter<TagRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.fragment_tag, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sponsor = paragrafList[position]

        holder.fill(sponsor)
    }

    override fun getItemCount(): Int = paragrafList.size

    class ViewHolder(private var tagView: View) : RecyclerView.ViewHolder(tagView) {

        fun fill(tag: Tag) {

            tagView.apply {
                tag_switch.text = tag.name
                tag_switch.isChecked = tag.isSubscribed

            }
        }
    }


}